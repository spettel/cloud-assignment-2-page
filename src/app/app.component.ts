import { Component } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {NumberService} from "./number.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(private numberService: NumberService) {
  }

  responseArray = []

  title = 'random-app';

  numRegex = "[0-9]*"

  numberForm = new FormGroup( {
    number: new FormControl("0", [
      Validators.required, Validators.min(0), Validators.max(20), Validators.pattern(this.numRegex)]
    )
  })

  onClickSubmit()
  {
    console.log("onclicksubmit")

    let number = this.numberForm.controls['number'].value

    this.numberService.getRandomNumbers(number).subscribe(
      (response) => {
        console.log("response received")
        console.log(response)
        this.responseArray = response
      },
      error => {
        console.log("received error")
        console.log(error)
      }
    )

  }
}
