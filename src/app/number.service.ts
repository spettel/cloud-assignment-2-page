import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpParams} from "@angular/common/http";
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class NumberService {

  url = "https://ductmvqe8g.execute-api.us-east-2.amazonaws.com/default/randomval"

  constructor(private http: HttpClient) { }

  getRandomNumbers(n : string) : Observable<any>
  {
    let params = new HttpParams().set("n", n);

    return this.http.get(this.url, {params : params} )
  }

  private handleError(error: HttpErrorResponse) {
    if (error.status === 0) {
      console.error('An error occurred:', error.error);
    } else {
      console.error(
        `Backend returned code ${error.status}, body was: `, error.error);
    }
  }

}
